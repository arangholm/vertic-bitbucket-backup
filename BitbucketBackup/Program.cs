﻿using BitbucketBackup.Properties;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitbucketBackup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var compositeLogger = new CompositeLogger())
            {
                foreach (string emailAddress in Settings.Default.BitbucketBackupAttemptRecipient)
                {

                    if (!String.IsNullOrEmpty(emailAddress))
                    {
                        compositeLogger.AddLogger(new EmailLogger(emailAddress));
                    }
                }

                compositeLogger.AddLogger(new ConsoleLogger());

                var kernel = new StandardKernel();

                kernel.Bind<IBitbucketBackup>().To<BitbucketBackup>();
                kernel.Bind<IBitbucketRequest>().To<BitbucketRequest>();
                kernel.Bind<IResponseParser>().To<ResponseParser>();
                kernel.Bind<IRepositoryUpdater>().To<RepositoryUpdater>();
                kernel.Bind<IRepositoryFactory>().To<RepositoryFactory>();
                kernel.Bind<IConfig>().To<Config>().InSingletonScope();
                kernel.Bind<ILogger>().ToConstant(compositeLogger);

                kernel.Get<IBitbucketBackup>().Execute();

                Console.WriteLine("Done.");

            }

        }
    }
}
